# Stickerify for Signal

This is a script to convert PNG and WEBP images for using them as stickers on [Signal](https://signal.org):
- resizes to 512x512 with a 16px padding
- adds a white bordure around the image (for good looking on dark theme)

## Installation

You will need some dependencies (command for Debian) :

```
apt install imagemagick wget bc gimp gimp-python
```

Then:

```
cd /tmp/
git clone https://framagit.org/luc/stickerify-for-signal.git
cd stickerify-for-signal
mkdir -p ~/.config/GIMP/2.10/plug-ins ~/.config/GIMP/2.10/scripts
cp batch-stickerify.scm ~/.config/GIMP/2.10/scripts
wget https://raw.githubusercontent.com/Psycojoker/sticker_bordure/35651e23880101958e2c41a4374e9386371942b9/stickers_bordure.py -O ~/.config/GIMP/2.10/plug-ins/stickers_bordure.py
chmod +x ~/.config/GIMP/2.10/plug-ins/stickers_bordure.py
sudo cp stickerify-for-signal /usr/local/bin
cd ..
rm -rf stickerify-for-signal
```

## Use

The stickers will be in a `converted` directory in the directory of the original images.

### Convert all the PNG and WEBP images of the current directory

```
stickerify-for-signal
```

### Convert all the PNG and WEBP images of a directory

```
stickerify-for-signal -d /path/to/directory
```

### Download PNG and WEBP images in a directory and convert them

First, create a file containing the URLs of the images to download (one URL per line).
Let’s say `urls.txt` for example:

```
stickerify-for-signal -u /path/to/urls.txt -d /path/to/directory
```

You can omit `-d /path/to/directory` if you want: images will be downloaded in current directory.

### Get help

```
stickerify-for-signal -h
```

## Upload your stickers

Please, follow Signal’s [Sticker Creator Guidelines](https://support.signal.org/hc/en-us/articles/360031836512-Stickers#h_c2a0a45b-862f-4d12-9ab1-d9a6844062ca).

After uploading your sticker pack, please consider adding them to <https://signalstickers.com>, which indexes Signal’s sticker packs.
For to do so, go on <https://signalstickers.com/contribute> and follow the instructions.

## Authors

[Luc Didry](https://fiat-tux.fr) for glueing a bunch of scripts and Gimp plug-in together, listed below:

- [Ondondil](https://github.com/ondondil) for the [original script](https://gist.github.com/ondondil/4b8564b404696b3255253b467b413de9#gistcomment-3118471) idea
- [Bram](https://worlddomination.be) for the [Gimp plug-in](https://github.com/Psycojoker/sticker_bordure)
- [Nels](https://framapiaf.org/@nels) for the [original Gimp script](https://framapiaf.org/@nels/103506549554308633)
- Fred Weinhaus which [ImageMagick squareup script](http://www.fmwconcepts.com/imagemagick/squareup/index.php) inspired the squaring part of the image processing

## Licence

WTFPL. See [LICENSE](LICENSE) file.
